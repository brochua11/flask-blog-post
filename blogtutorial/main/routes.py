from flask import Blueprint, abort, redirect, render_template, url_for
from markupsafe import escape

main = Blueprint('main', __name__, template_folder='templates', static_folder='static')

@main.route('/')
@main.route('/home')
def home():
    context_obj = {
        'title': 'Home',
        'subtitle': 'Blog post tutorial!'
    }
    return render_template('home.html', context=context_obj)

@main.route('/about')
def about():
    context_obj = {
        'title': 'About',
        'subtitle': 'Blog post tutorial!',
    }

    student = {
        'name': 'Axel',
        'course': 'Python Flask',
        'year': 2024,
        'sem' : 'Winter'
    }
    return render_template('about.html', context=context_obj, student=student)

@main.errorhandler(404)
def page_not_found(e):
    context_data = {
        'title': '404',
        'main_heading': 'File NOT Found'
    }

    return render_template('my-custm-404.html', context=context_data), 404


@main.route('/page-x/<string:next_page>')
def redirect_2_page(next_page):
    next_page = escape(next_page)

    if next_page == 'h':
        return redirect(url_for('home'))
    
    if next_page == 'a':
        return redirect(url_for('about'))
    
    return abort(404)
