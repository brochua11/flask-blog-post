from flask_wtf import FlaskForm
from wtforms import EmailField, PasswordField, StringField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, EqualTo

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(),
                                                   Length(min=2, max=25),])
    
    email = EmailField('Email', validators=[DataRequired(),])
    
    password = PasswordField('Password', validators=[DataRequired(), ])
    
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    
    submit = SubmitField('Register')

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(),
                                                   Length(min=2, max=25),])
    
    remember_me = BooleanField('Remember Me')
    
    password = PasswordField('Password', validators=[DataRequired(), ])
        
    submit = SubmitField('Login')