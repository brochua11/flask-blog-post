from flask import Blueprint, flash, render_template, url_for, redirect
from blogtutorial.auth.forms import RegistrationForm, LoginForm

auth = Blueprint('auth', __name__, template_folder='templates', static_folder='static')

@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    context_data = {
        'title': 'Sign up',
        'subtitle': 'Sign up with us!'
    }
    
    if form.validate_on_submit():
        # pseudo code for DB
        # user = db.session.query().filter_by(username=form.username.data)
        user_exists = False # Temporary to say user doesnt exist
        if not user_exists:
            # pseudo code for DB
            # new_user = User(username=form.username.data,
            #                 password=form.password.data,
            #                 email=form.email.data)
            # db.session.add(new_user)
            # db.session.commit()

            flash('User created successfully', 'success')
            return redirect(url_for('login'))
        else:
            flash('User already exists choose another username!', 'error')

    return render_template('register.html', context=context_data, form=form)

@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    context_data = {
        'title': 'Login',
        'subtitle': 'Login to your account!'
    }

    return render_template('login.html', context=context_data, form=form)
7