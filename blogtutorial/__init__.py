from flask import Flask
from blogtutorial.config import Config

def create_instance_app(class_config = Config):
    app = Flask(__name__)
    app.config['SECRET_KEY'] = class_config.SECRET_KEY

    from blogtutorial import main
    from blogtutorial import auth
    from blogtutorial import post

    app.register_blueprint(main)
    app.register_blueprint(auth)
    app.register_blueprint(post)

    return app

