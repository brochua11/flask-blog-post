import oracledb as ora
user, password, sn, host
if __name__ == '__main__':
    from config_db import user, password, sn, host
else:
    from blogtutorial.qdb.config_db import user, password, sn, host

conn = ora.connect(user=user, password=password, service_name=sn, host=host)