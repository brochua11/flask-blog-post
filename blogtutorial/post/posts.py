from blogtutorial.qdb.database import conn

def get_all_posts():
    keys = ('postId', 'author', 'title', 'content', 'postDate')
    cur = conn.cursor()
    qry = 'SELECT * FROM Posts'
    cur.execute(qry)
    posts = cur.fetchall()
    cur.close()

    post_dicts = []
    for post in posts:
        post_dict = {}
        for i in range(len(keys)):
            post_dict[keys[i]] = post[i]
        post_dicts.append(post_dict)
        
    return post_dicts

def get_post(post_id):
    for post in get_all_posts():
        if post['postId'] == post_id:
            return post