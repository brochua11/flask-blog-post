from flask import Blueprint, abort, render_template
from markupsafe import escape
from blogtutorial.post.posts import get_all_posts, get_post

post = Blueprint('post', __name__, static_folder='static', template_folder='templates')

@post.route('/post/<post_id>')
def post(post_id):
    post_id = escape(post_id)
    context_data = {
        'title': 'Post',
        'subtitle': f'Post information: {post_id}'
    }

    post = get_post(int(post_id))
    if post == None:
        abort(404)
    return render_template('post.html', context=context_data, post=post)

@post.route('/all-posts')
def all_posts():
    context_data = {
        'title': 'All Posts',
        'subtitle': 'Here are all the posts'
    }
    return render_template('allposts.html', context=context_data, post_list=get_all_posts())
