from blogtutorial import create_instance_app
from blogtutorial.config import Config

if __name__ == '__main__':
    app = create_instance_app(Config)
    app.run(port=5001, debug=True)